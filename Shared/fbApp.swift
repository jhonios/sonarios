//
//  fbApp.swift
//  Shared
//
//  Created by Janesh Suthar on 29/01/21.
//

import SwiftUI

@main
struct fbApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

